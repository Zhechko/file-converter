import * as fs from 'fs';

async function findFiles(directory: string): Promise<string[]> {
  try {
    let files: string[] = [];
    const directories = await fs.promises.readdir(directory, {
      withFileTypes: true,
    });
    for (const dir of directories) {
      const path = `${directory}/${dir.name}`;
      if (dir.isFile()) {
        files.push(path);
      } else if (dir.isDirectory()) {
        files = files.concat(await findFiles(path));
      }
    }
    return files;
  } catch (err) {
    console.error(`An error occurred: ${err}`);
    return Promise.reject(err);
  }
}


export default findFiles;
