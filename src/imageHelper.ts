const imageExtensions = ['jpg', 'jpeg', 'png', 'gif'];

export function isImage(fileName: string): boolean {
  const extension = fileName.split('.').pop();
  return imageExtensions.includes(extension || '');
}


export default {isImage};
