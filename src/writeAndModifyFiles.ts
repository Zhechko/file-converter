import * as fs from 'fs';
import {convertImageToWebp} from './webpImageConverter';
import {isImage} from './imageHelper';

async function writeAndModifyFiles(
  directory: string,
  files: string[],
): Promise<boolean> {
  try {
    if (!fs.existsSync(directory)) {
      await fs.promises.mkdir(directory);
    }
    for (const file of files) {
      const fileName = file.split('/').pop();

      if (isImage(fileName || '')) {
        if (!fileName) {
          continue;
        }
        const webpFileName = fileName.split('.')[0].concat('.webp') || '';
        const image = await convertImageToWebp(file);
        await fs.promises.writeFile(`${directory}/${webpFileName}`, image);
      } else {
        await fs.promises.copyFile(file, `${directory}/${fileName}`);
      }
    }
    return true;
  } catch (err) {
    console.error(`An error occurred: ${err}`);
    return false;
  }
}

export default writeAndModifyFiles;
