import writeAndModifyFiles from './writeAndModifyFiles';
import findFiles from './readFolderRoot';
import {createInterface} from "readline";

async function main() {


  const readline = createInterface({
    input: process.stdin,
    output: process.stdout
  });

  const readLineAsync = (msg: string) => {
    return new Promise(resolve => {
      readline.question(msg, userRes => {
        resolve(userRes);
      });
    });
  }

  const folderInputPath = await readLineAsync("Enter a input folder path: ");
  const folderOutputName = await readLineAsync("Enter a input folder name: ");

  readline.close();

  if (!folderInputPath && !folderOutputName) {
    console.log('Invalid input');
    return;
  }


  const myFolder = `${folderInputPath}/${folderOutputName}`;
  const files = await findFiles(folderInputPath as string);
  if (!files.length) {
    console.log('No files found.');
    return;
  }

  const result = await writeAndModifyFiles(myFolder, files);
  if (result) {
    console.log('Files created successfully.');
  }
}

main();
