import * as sharp from 'sharp';

export async function convertImageToWebp(filePath: string): Promise<Buffer> {
  try {
    const image = sharp(filePath);
    return await image.webp().toBuffer();
  } catch (err) {
    console.error(`An error occurred: ${err}`);
    throw err;
  }
}


export default {convertImageToWebp};
